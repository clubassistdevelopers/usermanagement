﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNetNuke.Web.Api;
//using DotNetNuke.Security;
using DotNetNuke.UI.UserControls;
using System.Net.Http;
using DotNetNuke.Entities.Users;
using System.Web.Http;
using DotNetNuke.Common.Utilities;
using Newtonsoft.Json;
using UserManagement.Model;
using System.Net;

namespace UserManagement.WebApi.controller
{
    [SupportedModules("UserManagement")]
    public class UserController : DnnApiController
    {
        [HttpGet]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public HttpResponseMessage GetCurrentUserInfo()
        {
            var user = DotNetNuke.Entities.Users.UserController.Instance.GetCurrentUserInfo();
            var usrInfo = new DNNUserInfo(user);
            var response = this.Request.CreateResponse(HttpStatusCode.OK, usrInfo);

            return response;
        }
    }
}
