﻿using DotNetNuke.Web.Api;
using System.Web.Http;

namespace UserManagement.WebApi
{
    public class ServiceRouteMapper : IServiceRouteMapper
    {
        public void RegisterRoutes(IMapRoute mapRouteManager)
        {
            mapRouteManager.MapHttpRoute("UserManagement", "default", "{controller}/{action}",
                new[] { "UserManagement.WebApi.controller" });
        }
    }
}
