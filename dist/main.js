(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".button, button, input[type='button'], input[type='reset'], input[type='submit'] {\r\n    background-color: #254768;\r\n}"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "    <div class=\"container\">\n      <!--<router-outlet></router-outlet>-->\n      <app-user-list></app-user-list>\n      <app-messages></app-messages>\n    </div>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'Provider Portal';
    }
    AppComponent.prototype.ngAfterViewInit = function () {
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _http_dnn_interceptor__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./http/dnn.interceptor */ "./src/app/http/dnn.interceptor.ts");
/* harmony import */ var _services_dnn_context_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./services/dnn-context.service */ "./src/app/services/dnn-context.service.ts");
/* harmony import */ var _users_user_edit_user_edit_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./users/user-edit/user-edit.component */ "./src/app/users/user-edit/user-edit.component.ts");
/* harmony import */ var _users_user_list_user_List_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./users/user-list/user-List.component */ "./src/app/users/user-list/user-List.component.ts");
/* harmony import */ var _env_service_provider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./env.service.provider */ "./src/app/env.service.provider.ts");
/* harmony import */ var _messages_messages_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./messages/messages.service */ "./src/app/messages/messages.service.ts");
/* harmony import */ var _messages_messages_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./messages/messages.component */ "./src/app/messages/messages.component.ts");
/* harmony import */ var _directives_display_directive__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./directives/display.directive */ "./src/app/directives/display.directive.ts");
/* harmony import */ var _pipes_displayrole_pipe__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./pipes/displayrole.pipe */ "./src/app/pipes/displayrole.pipe.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _messages_messages_component__WEBPACK_IMPORTED_MODULE_11__["MessagesComponent"],
                _users_user_edit_user_edit_component__WEBPACK_IMPORTED_MODULE_7__["UserEditComponent"],
                _users_user_list_user_List_component__WEBPACK_IMPORTED_MODULE_8__["UserListComponent"],
                _directives_display_directive__WEBPACK_IMPORTED_MODULE_12__["DisplayDirective"],
                _pipes_displayrole_pipe__WEBPACK_IMPORTED_MODULE_13__["DisplayRolePipe"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            ],
            providers: [
                _env_service_provider__WEBPACK_IMPORTED_MODULE_9__["EnvServiceProvider"],
                _messages_messages_service__WEBPACK_IMPORTED_MODULE_10__["MessagesService"],
                {
                    provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HTTP_INTERCEPTORS"],
                    useClass: _http_dnn_interceptor__WEBPACK_IMPORTED_MODULE_5__["DnnInterceptor"],
                    multi: true
                },
                _services_dnn_context_service__WEBPACK_IMPORTED_MODULE_6__["DnnContextService"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        }),
        __metadata("design:paramtypes", [])
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/directives/display.directive.ts":
/*!*************************************************!*\
  !*** ./src/app/directives/display.directive.ts ***!
  \*************************************************/
/*! exports provided: DisplayDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DisplayDirective", function() { return DisplayDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DisplayDirective = /** @class */ (function () {
    function DisplayDirective(el) {
        this.el = el;
    }
    DisplayDirective.prototype.display = function (visibility) {
        this.el.nativeElement.style.display = visibility;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('appDisplay'),
        __metadata("design:type", String)
    ], DisplayDirective.prototype, "displayValue", void 0);
    DisplayDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[appDisplay]'
        }),
        __metadata("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]])
    ], DisplayDirective);
    return DisplayDirective;
}());



/***/ }),

/***/ "./src/app/env.service.provider.ts":
/*!*****************************************!*\
  !*** ./src/app/env.service.provider.ts ***!
  \*****************************************/
/*! exports provided: EnvServiceFactory, EnvServiceProvider */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EnvServiceFactory", function() { return EnvServiceFactory; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EnvServiceProvider", function() { return EnvServiceProvider; });
/* harmony import */ var _env_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./env.service */ "./src/app/env.service.ts");

var EnvServiceFactory = function () {
    // Create env
    var env = new _env_service__WEBPACK_IMPORTED_MODULE_0__["EnvService"]();
    // Read environment variables from browser window
    var browserWindow = window || {};
    var browserWindowEnv = browserWindow['__env'] || {};
    for (var key in browserWindowEnv) {
        if (browserWindowEnv.hasOwnProperty(key)) {
            env[key] = window['__env'][key];
        }
    }
    return env;
};
var EnvServiceProvider = {
    provide: _env_service__WEBPACK_IMPORTED_MODULE_0__["EnvService"],
    useFactory: EnvServiceFactory,
    deps: [],
};


/***/ }),

/***/ "./src/app/env.service.ts":
/*!********************************!*\
  !*** ./src/app/env.service.ts ***!
  \********************************/
/*! exports provided: EnvService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EnvService", function() { return EnvService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var EnvService = /** @class */ (function () {
    function EnvService() {
        // The values that are defined here are the default values that can
        // be overridden by env.js
        // Whether or not to enable debug mode
        this.enableDebug = true;
        this.apiDnn = {
            //"url": "https://devmy.clubassist.com/",
            "url": "https://www.dnndev.me/",
            "path": "API/UserManagement/User/",
            "method": {
                "getCurrentUser": "GetCurrentUserInfo/"
            }
        };
        // API url 
        this.apiUserManagement = {
            "url": "https://stageapi.clubassist.com/UserManagement/",
            // TODO: the value below is a placeholder for the location of stageusermanagement
            // "url": "https://stageusermanagement.clubassist.com/",
            //"url": "https://www.dnndev.me/UserManagement/",
            "path": "api/Auth0User/",
            "method": {
                "getUsers": "GetUsers",
                "removeUser": "RemoveUser",
                "userUpdateRole": "UpdateUserRoles"
            }
        };
    }
    EnvService.prototype.getDocumentUrl = function () {
        return window.document.URL;
    };
    EnvService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], EnvService);
    return EnvService;
}());



/***/ }),

/***/ "./src/app/http/dnn.interceptor.ts":
/*!*****************************************!*\
  !*** ./src/app/http/dnn.interceptor.ts ***!
  \*****************************************/
/*! exports provided: DnnInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DnnInterceptor", function() { return DnnInterceptor; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_dnn_context_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/dnn-context.service */ "./src/app/services/dnn-context.service.ts");
/* harmony import */ var zone_js_dist_zone_error__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! zone.js/dist/zone-error */ "./node_modules/zone.js/dist/zone-error.js");
/* harmony import */ var zone_js_dist_zone_error__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(zone_js_dist_zone_error__WEBPACK_IMPORTED_MODULE_2__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DnnInterceptor = /** @class */ (function () {
    function DnnInterceptor(ctx) {
        this.ctx = ctx;
        this.context = this.ctx.getServiceFramework();
        console.log(this.context);
    }
    DnnInterceptor.prototype.intercept = function (request, next) {
        request = request.clone({
            setHeaders: {
                ModuleId: this.context.moduleId.toString(),
                TabId: this.context.tabId.toString(),
                RequestVerificationToken: this.context.antiForgeryToken
            }
        });
        return next.handle(request);
    };
    DnnInterceptor = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_services_dnn_context_service__WEBPACK_IMPORTED_MODULE_1__["DnnContextService"]])
    ], DnnInterceptor);
    return DnnInterceptor;
}());



/***/ }),

/***/ "./src/app/messages/messages.component.css":
/*!*************************************************!*\
  !*** ./src/app/messages/messages.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "h2 {\n    color: red;\n    font-family: Arial, Helvetica, sans-serif;\n    font-weight: lighter;\n  }\n  body {\n    margin: 2em;\n  }\n  body, input[text], button {\n    color: crimson;\n    font-family: Cambria, Georgia;\n  }\n  button.clear {\n    font-family: Arial;\n    background-color: #eee;\n    border: none;\n    padding: 5px 10px;\n    border-radius: 4px;\n    cursor: pointer;\n    cursor: hand;\n  }\n  button:hover {\n    background-color: #cfd8dc;\n  }\n  button:disabled {\n    background-color: #eee;\n    color: #aaa;\n    cursor: auto;\n  }\n  button.clear {\n    color: #888;\n    margin-bottom: 12px;\n  }"

/***/ }),

/***/ "./src/app/messages/messages.component.html":
/*!**************************************************!*\
  !*** ./src/app/messages/messages.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div></div>\n"

/***/ }),

/***/ "./src/app/messages/messages.component.ts":
/*!************************************************!*\
  !*** ./src/app/messages/messages.component.ts ***!
  \************************************************/
/*! exports provided: MessagesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MessagesComponent", function() { return MessagesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _messages_messages_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../messages/messages.service */ "./src/app/messages/messages.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MessagesComponent = /** @class */ (function () {
    function MessagesComponent(messagesService) {
        this.messagesService = messagesService;
    }
    MessagesComponent.prototype.ngOnInit = function () {
    };
    MessagesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-messages',
            template: __webpack_require__(/*! ./messages.component.html */ "./src/app/messages/messages.component.html"),
            styles: [__webpack_require__(/*! ./messages.component.css */ "./src/app/messages/messages.component.css")]
        }),
        __metadata("design:paramtypes", [_messages_messages_service__WEBPACK_IMPORTED_MODULE_1__["MessagesService"]])
    ], MessagesComponent);
    return MessagesComponent;
}());



/***/ }),

/***/ "./src/app/messages/messages.service.ts":
/*!**********************************************!*\
  !*** ./src/app/messages/messages.service.ts ***!
  \**********************************************/
/*! exports provided: MessagesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MessagesService", function() { return MessagesService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var MessagesService = /** @class */ (function () {
    function MessagesService() {
        this.messages = [];
    }
    /**
    * Post any type of informational message from the application to the screen.
    * @param { message } [string] - string to post to the screen
    */
    MessagesService.prototype.add = function (message) {
        this.messages.push(message);
    };
    MessagesService.prototype.clear = function () {
        this.messages = [];
    };
    MessagesService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        })
    ], MessagesService);
    return MessagesService;
}());



/***/ }),

/***/ "./src/app/pipes/displayrole.pipe.ts":
/*!*******************************************!*\
  !*** ./src/app/pipes/displayrole.pipe.ts ***!
  \*******************************************/
/*! exports provided: DisplayRolePipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DisplayRolePipe", function() { return DisplayRolePipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var DisplayRolePipe = /** @class */ (function () {
    function DisplayRolePipe() {
    }
    /**
    * Handle display of user role.
    * @param { string } roleVal - colon separated value representing system:role
    * @returns { string } - the value of the role from array index 1
    */
    DisplayRolePipe.prototype.transform = function (roleVal) {
        if (roleVal) {
            var roleArray = roleVal.split(',');
            if (roleArray.length === 1 && roleArray[0].startsWith("EInvoice:")) {
                return roleVal.split(':')[1];
            }
            else if (roleArray.length > 1) {
                var roles = roleArray.filter(this.containsEinvoiceRole);
                return roles[0].split(':')[1];
            }
        }
        //return roleVal.startsWith("EInvoice:") ? roleVal.split(':')[1] : "";
        //return roleVal.split(':')[1];
    };
    DisplayRolePipe.prototype.containsEinvoiceRole = function (role) {
        return role.startsWith("EInvoice:");
    };
    DisplayRolePipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({ name: 'displayRole' })
    ], DisplayRolePipe);
    return DisplayRolePipe;
}());



/***/ }),

/***/ "./src/app/services/api.service.ts":
/*!*****************************************!*\
  !*** ./src/app/services/api.service.ts ***!
  \*****************************************/
/*! exports provided: ApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiService", function() { return ApiService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_dnn_context_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/dnn-context.service */ "./src/app/services/dnn-context.service.ts");
/* harmony import */ var _env_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../env.service */ "./src/app/env.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ApiService = /** @class */ (function () {
    function ApiService(ctx, env, http) {
        this.ctx = ctx;
        this.env = env;
        this.http = http;
        this._opts = {
            headers: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"],
            params: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]
        };
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/json', 'Cache-Control': 'no-cache' });
        this._opts.headers = this.headers;
        this._opts.params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]();
    }
    /**
     * Handle Http get operation.
     * @param { url } [string] - name of the operation that failed
     * @param { param } [any] - optional values passed into the API
     */
    ApiService.prototype.get = function (url, param) {
        this._opts.params = {};
        if (param != null) {
            var params = param;
            for (var key in param) {
                if (param.hasOwnProperty(key)) {
                    var val = param[key];
                    // this._opts.params.set(key, val);
                }
            }
            this._opts.params = params;
        }
        return this.http.get(url, this._opts)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError('get')));
    };
    /**
 * Handle Http post operation.
 * @param { url } [string] - URL of API
 * @param { body } [any] - name of the operation
 * @param { param } [any] - optional values passed into the API
 */
    ApiService.prototype.post = function (url, body, param) {
        this._opts.params = {};
        if (param != null) {
            var params = param;
            this._opts.params = params;
        }
        return this.http.post(url, body, this._opts)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError('post')));
    };
    /**
     * Handle Http put operation.
     * @param { url } [string] - URL of API
     * @param { body } [any] - name of the operation
     * @param { param } [any] - optional values passed into the API
     */
    ApiService.prototype.put = function (url, body, param) {
        this._opts.params = {};
        if (param != null) {
            var params = param;
            this._opts.params = params;
        }
        return this.http.put(url, body, this._opts)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError('get')));
    };
    /**
     * Handle Http delete operation.
     * @param { url } [string] - URL of API
     * @param { param } [any] - optional values passed into the API
     */
    ApiService.prototype.delete = function (url, param) {
        this._opts.params = {};
        if (param != null) {
            var params = param;
            this._opts.params = params;
        }
        return this.http.delete(url, this._opts)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError('get')));
    };
    /**
   * Handle Http get operation.
   * @param { url } [string] - URL of API
   * @param { param } [any] - optional values passed into the API
   */
    ApiService.prototype.getBody = function (url, param) {
        // this._token = from(this.loginService.login());
        if (param != null) {
            var params = param;
            this._opts.params = params;
        }
        return this.http.get(url, this._opts)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError('get')));
    };
    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    ApiService.prototype.handleError = function (operation, result) {
        var _this = this;
        if (operation === void 0) { operation = 'operation'; }
        return function (error) {
            // TODO: error message formatting
            _this.log(operation + " failed: " + error.message);
            // Let the app keep running by returning an empty result.
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])(result);
        };
    };
    /** Log a status message with the MessageService */
    ApiService.prototype.log = function (message) {
        console.log('API Service Message: ' + message);
    };
    ApiService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_services_dnn_context_service__WEBPACK_IMPORTED_MODULE_4__["DnnContextService"],
            _env_service__WEBPACK_IMPORTED_MODULE_5__["EnvService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], ApiService);
    return ApiService;
}());



/***/ }),

/***/ "./src/app/services/contexts/dev-context.ts":
/*!**************************************************!*\
  !*** ./src/app/services/contexts/dev-context.ts ***!
  \**************************************************/
/*! exports provided: DevContext */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DevContext", function() { return DevContext; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var DevContext = /** @class */ (function () {
    function DevContext() {
        this.ignoreMissing$2sxc = false;
        this.ignoreMissingServicesFramework = false;
        this.forceUse = false;
        this.moduleId = 0;
        this.tabId = 0;
        this.path = '/';
    }
    DevContext = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])()
    ], DevContext);
    return DevContext;
}());



/***/ }),

/***/ "./src/app/services/dnn-context.service.ts":
/*!*************************************************!*\
  !*** ./src/app/services/dnn-context.service.ts ***!
  \*************************************************/
/*! exports provided: DnnContextService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DnnContextService", function() { return DnnContextService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _contexts_dev_context__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./contexts/dev-context */ "./src/app/services/contexts/dev-context.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





var DnnContextService = /** @class */ (function () {
    function DnnContextService(devSettings) {
        this.devSettings = devSettings;
        this.tidSubject = new rxjs__WEBPACK_IMPORTED_MODULE_2__["ReplaySubject"](1);
        this.afTokenSubject = new rxjs__WEBPACK_IMPORTED_MODULE_2__["ReplaySubject"](1);
        this._moduleId = -1;
        this._tabId = -1;
        this._antiForgeryToken = '';
        this._path = 'UserManagement';
        this._baseUrl = '';
        this._properties = {};
        this._userId = "";
        this.initComplete = false;
        this.tabId$ = this.tidSubject.asObservable();
        this.antiForgeryToken$ = this.afTokenSubject.asObservable();
        this.all$ = Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["combineLatest"])(this.tabId$, // wait for tabId
        this.antiForgeryToken$) // wait for security token
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return ({
            tabId: res[0],
            antiForgeryToken: res[1]
        }); }));
        var MODULE = 'UserManagement';
        this.devSettings = Object.assign({}, {
            ignoreMissing$2sxc: false,
            ignoreMissingServicesFramework: false
        }, devSettings);
        if (window && window[MODULE]) {
            this._properties = window[MODULE];
            console.log('​-----------------------------------------------------------------------');
            console.log('​DnnContextService -> constructor -> this._properties', this._properties);
            console.log('​-----------------------------------------------------------------------');
        }
        else {
            console.log('----------------------');
            console.log('ERROR: Missing window[MODULE] for DNN');
            console.log('----------------------');
        }
    }
    DnnContextService.prototype.ngOnInit = function () {
        this.initComplete = true;
    };
    DnnContextService.prototype.getServiceFramework = function () {
        this._moduleId = this._properties.ModuleId;
        if (this._antiForgeryToken !== '') {
            return this.context;
        }
        else {
            // Check if DNN Services framework exists.
            if (window.$ && window.$.ServicesFramework) {
                var sf = window.$.ServicesFramework(this._moduleId);
                // Check if sf is initialized.
                if (sf.getAntiForgeryValue() && sf.getTabId() !== -1) {
                    this._tabId = sf.getTabId();
                    this._antiForgeryToken = sf.getAntiForgeryValue();
                    this._baseUrl = sf.getServiceRoot(this._path);
                    return this.context;
                }
                else {
                    return null;
                }
            }
            else {
                return null;
            }
        }
    };
    Object.defineProperty(DnnContextService.prototype, "initCompleted", {
        get: function () {
            return this.initComplete;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DnnContextService.prototype, "context", {
        get: function () {
            return { 'tabId': this._tabId, 'antiForgeryToken': this._antiForgeryToken, 'moduleId': this._moduleId };
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DnnContextService.prototype, "properties", {
        get: function () {
            return this._properties;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DnnContextService.prototype, "resources", {
        get: function () {
            return this._properties.Resources;
        },
        enumerable: true,
        configurable: true
    });
    DnnContextService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"])()),
        __metadata("design:paramtypes", [_contexts_dev_context__WEBPACK_IMPORTED_MODULE_1__["DevContext"]])
    ], DnnContextService);
    return DnnContextService;
}());



/***/ }),

/***/ "./src/app/users/model/dnnuser.model.ts":
/*!**********************************************!*\
  !*** ./src/app/users/model/dnnuser.model.ts ***!
  \**********************************************/
/*! exports provided: DnnUser */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DnnUser", function() { return DnnUser; });
var DnnUser = /** @class */ (function () {
    function DnnUser() {
    }
    return DnnUser;
}());



/***/ }),

/***/ "./src/app/users/model/user.model.ts":
/*!*******************************************!*\
  !*** ./src/app/users/model/user.model.ts ***!
  \*******************************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
var User = /** @class */ (function () {
    function User(auth0UserID, firstName, lastName, email, roles) {
    }
    return User;
}());



/***/ }),

/***/ "./src/app/users/user-edit/user-edit.component.css":
/*!*********************************************************!*\
  !*** ./src/app/users/user-edit/user-edit.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/users/user-edit/user-edit.component.html":
/*!**********************************************************!*\
  !*** ./src/app/users/user-edit/user-edit.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "      <div class=\"modal-body\">\n        <form #userForm=\"ngForm\" (ngSubmit)=\"onSubmit()\" name=\"userForm\">\n          <input type=\"hidden\" name=\"auth0UserID\" [(ngModel)]=\"user.auth0UserID\">\n            <div class=\"form-group\">\n              <label for=\"firstName\">First Name</label>\n              <input type=\"text\" class=\"form-control\" [(ngModel)]=\"user.firstName\" name=\"firstName\" readonly>\n             </div>\n            <div class=\"form-group\">\n              <label for=\"lastName\">Last Name</label>\n              <input type=\"text\" class=\"form-control\" [(ngModel)]=\"user.lastName\" name=\"lastName\" readonly>\n             </div>\n            <div class=\"form-group\">\n              <label for=\"email\">Email</label>\n              <input type=\"email\" class=\"form-control\" [(ngModel)]=\"user.email\" name=\"email\" readonly>\n            </div>\n            <input type=\"hidden\" name=\"roles\" [(ngModel)]=\"user.roles\">\n            <fieldset class=\"form-group\">\n              <div class=\"form-check col-md-auto\">\n                <input class=\"form-check-input\" id=\"Manager\" type=\"radio\" value=\"EInvoice:Manager\" name=\"role\"\n                [checked]=\"currentUserContainsRole('EInvoice:Manager')\" (change)=\"popOrSpliceRole($event.target, 'EInvoice:Manager')\">              \n                <label class=\"form-check-label\" for=\"userRole-Manager\">Manager</label>\n              </div>\n              <div class=\"form-check col-md-auto\">\n                <input class=\"form-check-input\" id=\"Administrator\" type=\"radio\" value=\"EInvoice:Administrator\" name=\"role\"\n                [checked]=\"currentUserContainsRole('EInvoice:Administrator')\" (change)=\"popOrSpliceRole($event.target, 'EInvoice:Administrator')\">              \n                <label class=\"form-check-label\" for=\"userRole-Administrator\">Administrator</label>\n              </div>\n              <div class=\"form-check col-md-auto\">\n                <input class=\"form-check-input\" id=\"eInvoiceUser\" type=\"radio\" value=\"EInvoice:eInvoice\" name=\"role\"\n                [checked]=\"currentUserContainsRole('EInvoice:eInvoice')\" (change)=\"popOrSpliceRole($event.target, 'EInvoice:eInvoice')\">              \n                <label class=\"form-check-label\" for=\"userRole-eInvoiceUser\">eInvoice</label>\n              </div>\n              <div class=\"form-check col-md-auto\">\n                <input class=\"form-check-input\" id=\"Technicial\" type=\"radio\" value=\"EInvoice:Technician\" name=\"role\"\n                [checked]=\"currentUserContainsRole('EInvoice:Technician')\" (change)=\"popOrSpliceRole($event.target, 'EInvoice:Technician')\">              \n                <label class=\"form-check-label\" for=\"userRole-Technician\">Technician</label>\n              </div>\n            </fieldset>\n            <div class=\"modal-footer mt-3\">\n              <button type=\"reset\" data-dismiss=\"modal\" class=\"btn btn-sm btn-danger mr-1\">Cancel</button>\n              <button type=\"submit\" data-dismiss=\"modal\"  class=\"btn btn-sm btn-primary\" (click)=\"saveUser();\">Save Changes</button>\n            </div>\n          </form>\n      </div>\n\n  "

/***/ }),

/***/ "./src/app/users/user-edit/user-edit.component.ts":
/*!********************************************************!*\
  !*** ./src/app/users/user-edit/user-edit.component.ts ***!
  \********************************************************/
/*! exports provided: UserEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserEditComponent", function() { return UserEditComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _users_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../users.service */ "./src/app/users/users.service.ts");
/* harmony import */ var _model_user_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../model/user.model */ "./src/app/users/model/user.model.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UserEditComponent = /** @class */ (function () {
    function UserEditComponent(userService) {
        this.userService = userService;
        this.submitted = false;
    }
    UserEditComponent.prototype.onSubmit = function () { this.submitted = true; };
    UserEditComponent.prototype.ngOnInit = function () {
    };
    UserEditComponent.prototype.selectOneRadio = function (value) {
        jquery__WEBPACK_IMPORTED_MODULE_1__("input:checked").prop('checked', false);
        jquery__WEBPACK_IMPORTED_MODULE_1__('#' + value).prop('checked', true);
    };
    UserEditComponent.prototype.currentUserContainsRole = function (role) {
        var userHasRole = false;
        if (this.user && this.user.roles !== undefined) {
            userHasRole = this.user.roles.includes(role);
        }
        return userHasRole;
    };
    UserEditComponent.prototype.userContainsRole = function (curUser, role) {
        var userHasRole = false;
        if (curUser && curUser.roles !== undefined) {
            userHasRole = curUser.roles.includes(role);
        }
        return userHasRole;
    };
    UserEditComponent.prototype.popOrSpliceRole = function (target, role) {
        this.oldRoles = this.user.roles.slice();
        var userRoles = this.clearEinvoiceRoles();
        if (target.checked && !this.userContainsRole(this.user, target.value)) {
            userRoles.push(role);
            this.user.roles = userRoles.toString().trim();
        }
        else if (!target.checked) {
            var idx = userRoles.indexOf(target.value, 0);
            if (idx >= 0)
                userRoles.splice(idx, 1);
            this.user.roles = userRoles.toString().trim();
        }
    };
    UserEditComponent.prototype.clearEinvoiceRoles = function () {
        var userRoles = this.user.roles.split(',');
        var i = 0;
        userRoles.forEach(function (element) {
            if (element.startsWith("EInvoice"))
                userRoles.splice(i, 1);
            i++;
        });
        return userRoles;
    };
    UserEditComponent.prototype.saveUser = function () {
        var _this = this;
        console.log("Save User: " + JSON.stringify(this.user));
        var rolesAssignment = {
            "Auth0UserID": this.user.auth0UserID,
            "RolesCsv": this.user.roles.toString()
        };
        this.userService.userUpdateRole(rolesAssignment)
            .subscribe(function (data) { return _this.user = data; });
    };
    Object.defineProperty(UserEditComponent.prototype, "diagnostic", {
        get: function () { return JSON.stringify(this.user); },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", _model_user_model__WEBPACK_IMPORTED_MODULE_3__["User"])
    ], UserEditComponent.prototype, "user", void 0);
    UserEditComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-user-edit',
            template: __webpack_require__(/*! ./user-edit.component.html */ "./src/app/users/user-edit/user-edit.component.html"),
            styles: [__webpack_require__(/*! ./user-edit.component.css */ "./src/app/users/user-edit/user-edit.component.css")]
        }),
        __metadata("design:paramtypes", [_users_service__WEBPACK_IMPORTED_MODULE_2__["UsersService"]])
    ], UserEditComponent);
    return UserEditComponent;
}());



/***/ }),

/***/ "./src/app/users/user-list/user-List.component.ts":
/*!********************************************************!*\
  !*** ./src/app/users/user-list/user-List.component.ts ***!
  \********************************************************/
/*! exports provided: UserListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserListComponent", function() { return UserListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _users_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../users.service */ "./src/app/users/users.service.ts");
/* harmony import */ var _model_user_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../model/user.model */ "./src/app/users/model/user.model.ts");
/* harmony import */ var _model_dnnuser_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../model/dnnuser.model */ "./src/app/users/model/dnnuser.model.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

//import { Router } from '@angular/router';




var UserListComponent = /** @class */ (function () {
    function UserListComponent(userService) {
        this.userService = userService;
        this.page = 0;
        this.perPage = 100;
        this.user = new _model_user_model__WEBPACK_IMPORTED_MODULE_3__["User"]("", "", "", "", "");
        this.isUserSelected = false;
        this.dnnuser = new _model_dnnuser_model__WEBPACK_IMPORTED_MODULE_4__["DnnUser"]();
        this.users = [];
    }
    UserListComponent.prototype.ngOnInit = function () {
        this.getCurrentDnnUserInfo();
    };
    UserListComponent.prototype.ngAfterViewInit = function () {
    };
    UserListComponent.prototype.getCurrentDnnUserInfo = function () {
        var _this = this;
        this.userService.getCurrentDnnUserInfo()
            .subscribe(function (res) { return _this.setCurrentDnnUserInfo(res); }, function (err) { return console.error(err); });
    };
    UserListComponent.prototype.setCurrentDnnUserInfo = function (user) {
        this.dnnuser = user;
        this.getUsers();
    };
    UserListComponent.prototype.setCurrentUser = function (user) {
        this.user = user;
        console.log("setUserData");
    };
    UserListComponent.prototype.getUsers = function () {
        var _this = this;
        var customerKey = this.dnnuser.customerkeylist.split(",")[0];
        this.userService.getUsers(customerKey, this.page, this.perPage)
            .subscribe(function (data) { return _this.users = data; });
    };
    UserListComponent.prototype.removeUser = function () {
        var _this = this;
        if (this.isUserSelected) {
            this.userService.removeUser(this.selectedUser.auth0UserID)
                .subscribe(function (data) {
                if (data !== undefined) {
                    _this.getUsers();
                }
            });
        }
    };
    UserListComponent.prototype.clearSelectedUser = function () {
        if (this.isUserSelected) {
            this.isUserSelected = false;
            this.selectedUser = new _model_user_model__WEBPACK_IMPORTED_MODULE_3__["User"]("", "", "", "", "");
        }
    };
    UserListComponent.prototype.openWarnModal = function (user) {
        this.selectedUser = user;
        this.isUserSelected = true;
    };
    UserListComponent.prototype.openUserModal = function () {
        jquery__WEBPACK_IMPORTED_MODULE_1__('#UserEditModal').modal('show');
    };
    Object.defineProperty(UserListComponent.prototype, "diagnostic", {
        get: function () { return JSON.stringify(this.user); },
        enumerable: true,
        configurable: true
    });
    UserListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-user-list',
            template: __webpack_require__(/*! ./user-list.component.html */ "./src/app/users/user-list/user-list.component.html"),
            styles: [__webpack_require__(/*! ./user-list.component.css */ "./src/app/users/user-list/user-list.component.css")]
        }),
        __metadata("design:paramtypes", [_users_service__WEBPACK_IMPORTED_MODULE_2__["UsersService"]])
    ], UserListComponent);
    return UserListComponent;
}());



/***/ }),

/***/ "./src/app/users/user-list/user-list.component.css":
/*!*********************************************************!*\
  !*** ./src/app/users/user-list/user-list.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/users/user-list/user-list.component.html":
/*!**********************************************************!*\
  !*** ./src/app/users/user-list/user-list.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row pb-3\">&nbsp;</div>\n<div class=\"row border shadow-sm pt-2 pb-2\">\n  <div class=\"col align-self-start\"><strong>Name</strong></div>\n  <div class=\"col align-self-start\"><strong>Email</strong></div>\n  <div class=\"col align-self-start\"><strong>Role</strong></div>\n  <div class=\"col-md-2 align-self-end\">&nbsp;</div>\n</div>\n  \n<div class=\"row border-bottom pt-2 pb-2\" *ngFor=\"let u of users; let idx = index\" [class.selected]=\"u === user\">\n  <p *ngIf=\"users.length <= 0\">No users available for this station.</p>\n  <div class=\"col\">{{u.firstName}} {{u.lastName}} </div>\n  <div class=\"col\">{{u.email}}</div>\n  <div class=\"col align-self-end\">\n    {{u.roles | displayRole }}\n  </div>\n  <div class=\"col-md-2 align-self-end\">\n    <span class=\"pr-3\" data-toggle=\"modal\" data-target=\"#UserModal\" style=\"cursor: pointer\" (click)=\"this.setCurrentUser(u)\">\n      <svg width=\"1em\" height=\"1em\" viewBox=\"0 0 16 16\" class=\"bi bi-pencil-square text-success\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\">\n        <path d=\"M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z\"/>\n        <path fill-rule=\"evenodd\" d=\"M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z\"/>\n      </svg>\n    </span>\n    <span style=\"cursor: pointer\" data-toggle=\"modal\" data-target=\"#WarnModal\" data-action=\"user-delete\" (click)=\"openWarnModal(u)\">\n    <svg width=\"1em\" height=\"1em\" viewBox=\"0 0 16 16\" class=\"bi bi-trash text-danger\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\">\n      <path d=\"M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z\"/>\n      <path fill-rule=\"evenodd\" d=\"M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z\"/>\n    </svg>\n  </span>\n  </div>\n</div>\n<!--\n  ********************************************************\n  * TODO: Add Pagination when API contains funcitonality *\n  ********************************************************\n<div class=\"row border-bottom pt-2 pb-2 justify-content-end\">\n  <nav class=\"col\" aria-label=\"Page Navigation\">\n    <ul class=\"pagination justify-content-end\">\n      <li class=\"page-item disabled\"><a class=\"page-link\" href=\"#\">Previous</a></li>\n      <li class=\"page-item active\"><a class=\"page-link\" href=\"#\">1</a></li>\n      <li class=\"page-item\"><a class=\"page-link\" href=\"#\">2</a></li>\n      <li class=\"page-item\"><a class=\"page-link\" href=\"#\">3</a></li>\n      <li class=\"page-item\"><a class=\"page-link\" href=\"#\">Next</a></li>\n    </ul>\n  </nav>\n</div>\n-->\n<div class=\"modal fade\" id=\"WarnModal\" tabindex=\"-1\" role=\"dialog\" data-backdrop=\"static\" aria-labelledby=\"delModalTitle\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-md modal-dialog-centered\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h5 class=\"modal-title\" id=\"delModalTitle\">Delete User</h5>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\">\n        <p>Are you sure you want to delete this user?</p>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" data-dismiss=\"modal\" class=\"btn btn-sm btn-danger mr-1\" (click)=\"clearSelectedUser();\">Cancel</button>\n        <button type=\"button\" data-dismiss=\"modal\" class=\"btn btn-sm btn-success\" (click)=\"removeUser();\">Yes</button>\n      </div>\n    </div>\n  </div>\n</div>\n\n<div class=\"modal fade\" id=\"UserModal\" tabindex=\"-1\" role=\"dialog\" data-backdrop=\"static\" aria-labelledby=\"userModalTitle\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-md modal-dialog-centered\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h5 class=\"modal-title\" id=\"userModalTitle\">User Detail</h5>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\">\n        <app-user-edit [user]=\"user\"></app-user-edit>\n      </div>\n    </div>\n  </div>\n</div>\n  \n\n"

/***/ }),

/***/ "./src/app/users/users.service.ts":
/*!****************************************!*\
  !*** ./src/app/users/users.service.ts ***!
  \****************************************/
/*! exports provided: UsersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersService", function() { return UsersService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _services_dnn_context_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/dnn-context.service */ "./src/app/services/dnn-context.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var _env_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../env.service */ "./src/app/env.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





//import { USERS } from './model/mock_user_data';

var UsersService = /** @class */ (function () {
    function UsersService(http, ctx, apiService, env) {
        this.http = http;
        this.ctx = ctx;
        this.apiService = apiService;
        this.env = env;
        this._urlBase = env.apiUserManagement.url;
        this._dnnApiBase = this.getModuleUrl();
        this._dnnPath = this.ctx._properties.routingWebAPI;
        console.log(this._urlBase);
    }
    UsersService.prototype.getModuleUrl = function () {
        return this.env.getDocumentUrl();
    };
    UsersService.prototype.getLoggedInUserInfo = function () {
        console.log("GetCurrentUserInfo");
        return this.apiService.get(this.env.apiDnn.path + this.env.apiDnn.method.getCurrentUser)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (data) { return data; }));
    };
    UsersService.prototype.removeUser = function (auth0userid) {
        console.log("removeUser");
        return this.apiService.post(this._urlBase + this.env.apiUserManagement.path + this.env.apiUserManagement.method.removeUser, { "auth0UserID": auth0userid });
    };
    UsersService.prototype.userUpdateRole = function (rolesAssignment) {
        console.log("userUpdateRole");
        return this.apiService.put(this._urlBase + this.env.apiUserManagement.path + this.env.apiUserManagement.method.userUpdateRole, rolesAssignment)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (data) { return data; }));
    };
    UsersService.prototype.getUsers = function (customerKey, page, perPage) {
        console.log("getUsers");
        //return of(USERS);
        return this.apiService.get(this._urlBase + this.env.apiUserManagement.path + this.env.apiUserManagement.method.getUsers + '/' + page + '/' + perPage + '/' + customerKey);
    };
    UsersService.prototype.getUser = function () {
        console.log("getUsers");
        return this.apiService.get(this._urlBase + this.env.apiUserManagement.path + this.env.apiUserManagement.method.getUsers)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (data) { return data; }));
    };
    UsersService.prototype.getCurrentDnnUserInfo = function () {
        console.log("GetCurrentUserInfo");
        return this.http.get(this._dnnPath + '/User/' + this.env.apiDnn.method.getCurrentUser)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (data) { return data; }));
    };
    UsersService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root',
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _services_dnn_context_service__WEBPACK_IMPORTED_MODULE_2__["DnnContextService"],
            _services_api_service__WEBPACK_IMPORTED_MODULE_4__["ApiService"],
            _env_service__WEBPACK_IMPORTED_MODULE_5__["EnvService"]])
    ], UsersService);
    return UsersService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\Source\repos\providerportal\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map