﻿using DotNetNuke.ComponentModel.DataAnnotations;
using System;
using System.Security.Cryptography.X509Certificates;
using System.Web.Caching;
using DotNetNuke.Entities.Users;
using Newtonsoft.Json;

namespace UserManagement.Model
{
    [JsonObject(MemberSerialization.OptIn)]
    public class DNNUserInfo
    {

        public DNNUserInfo(UserInfo t)
        {
            UserID = t.UserID;
            PortalID = t.PortalID;
            Username = t.Username;
            FirstName = t.FirstName;
            LastName = t.LastName;
            Email = t.Email;
            Roles = t.Roles;
            ClubList = t.Profile.ProfileProperties["ClubIDCSV"].PropertyValue.ToString();
            CustomerKeyList = t.Profile.ProfileProperties["CustomerKeyCSV"].PropertyValue.ToString();
        }

        public DNNUserInfo() { }

        [JsonProperty("userid")]
        public int UserID { get; set; }

        [JsonProperty("portalid")]
        public int PortalID { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("displayname")]
        public string DisplayName { get; set; }

        [JsonProperty("firstname")]
        public string FirstName { get; set; }

        [JsonProperty("lastname")]
        public string LastName { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("roles")]
        public string[] Roles { get; set; }

        [JsonProperty("clublist")]
        public string ClubList { get; set; }

        [JsonProperty("customerkeylist")]
        public string CustomerKeyList { get; set; }
    }
}